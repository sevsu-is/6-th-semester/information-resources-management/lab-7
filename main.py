from mysql.connector import connect, Error
from mysql.connector.abstracts import MySQLConnectionAbstract
from mysql.connector.pooling import PooledMySQLConnection


def read(male_connection: MySQLConnectionAbstract | PooledMySQLConnection,
         female_connection: MySQLConnectionAbstract | PooledMySQLConnection):
    print()
    print("Data from server 1 (MALE):")
    with male_connection.cursor(buffered=True) as male_cursor:
        male_cursor.execute("SELECT * FROM people")
        for db in male_cursor:
            print(db)

    print()
    print("Data from server 2 (FEMALE):")
    with female_connection.cursor(buffered=True) as female_cursor:
        female_cursor.execute("SELECT * FROM people")
        for db in female_cursor:
            print(db)


def write(male_connection: MySQLConnectionAbstract | PooledMySQLConnection,
          female_connection: MySQLConnectionAbstract | PooledMySQLConnection):
    print()
    print('Fill in the data: (Name, Gender, Profession, Email)')
    provided_input = input()
    name, gender, profession, email = provided_input.split(sep=', ')

    if gender == 'M':
        with male_connection.cursor() as male_cursor:
            male_cursor.execute("""INSERT INTO people(name, gender, profession, email) VALUES(%s, %s, %s, %s);""",
                                (name, gender, profession, email))
            male_connection.commit()
    else:
        with female_connection.cursor() as female_cursor:
            female_cursor.execute("""INSERT INTO people(name, gender, profession, email) VALUES(%s, %s, %s, %s);""",
                                  (name, gender, profession, email))
            female_connection.commit()


def find_by_email(male_connection: MySQLConnectionAbstract | PooledMySQLConnection,
               female_connection: MySQLConnectionAbstract | PooledMySQLConnection):
    print()
    print('Provide the email:')
    email = input()

    with male_connection.cursor(buffered=True) as male_cursor:
        male_cursor.execute("""SELECT * FROM people WHERE email = %s;""", [email])
        for db in male_cursor:
            print(str(db) + " found on MALE server (server - 1)")

    with female_connection.cursor(buffered=True) as female_cursor:
        female_cursor.execute("""SELECT * FROM people WHERE email = %s;""", [email])
        for db in female_cursor:
            print(str(db) + " found on FEMALE server (server - 1)")


def main():
    try:
        with connect(host="localhost", user="root", password="", database="uir-lab7", port=3308) as male_connection:
            with connect(host="localhost", user="root", password="", database="uir-lab7",
                         port=3309) as female_connection:
                while True:
                    print()
                    print("SELECT MODE:")
                    print("1. Read")
                    print("2. Write")
                    print("3. Find by email")
                    print("q. Exit")
                    mode = input()

                    if mode == '1':
                        read(male_connection, female_connection)
                    elif mode == '2':
                        write(male_connection, female_connection)
                    elif mode == '3':
                        find_by_email(male_connection, female_connection)
                    elif mode == 'q':
                        break
    except Error as e:
        print(e)


if __name__ == '__main__':
    main()
