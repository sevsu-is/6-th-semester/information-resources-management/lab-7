CREATE TABLE IF NOT EXISTS `people` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(40) NOT NULL DEFAULT 'Ivan Ivanov',
    `gender` VARCHAR(1) NOT NULL,
    `profession` VARCHAR(40) NOT NULL DEFAULT 'developer',
    `email` VARCHAR(40) NOT NULL DEFAULT 'ivan.ivanov@mail.ru',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB;